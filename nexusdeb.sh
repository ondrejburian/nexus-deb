#!/bin/bash
# nexusdeb builds a debian package of the Nexus repository manager. nexusdeb
# downloads nexus by itself. You run it by
#   nexusdeb.sh <version> <maintainer>
# Example:
#   nexusdeb.sh 3.25.0-03 "Ondrej Burian" ondrej.burian@ultimum.io
#
# The script has been tested with version 2.0.5.

if [ -z $1 ]
then
	echo "Usage: nexusdeb.sh <Version> <Maintainer>:"
	echo -e "Example: nexusdeb.sh 3.25.0-03 'Ondrej Burian' ondrej.burian@ultimum.io"
	exit 1
fi

#if [ -z $2 ]
#then
#	echo "The maintainer is missing. Please provide it as second argument."
#	exit 1
#fi
#
#if [ -z $3 ]
#then
#	echo "The maintainer email is missing. Please provide it as third argument."
#	exit 1
#fi

export DEBFULLNAME="Ondrej Burian"
export DEBEMAIL="ondrej.burian@ultimum.io"

set -e

rm -rf nexus || true
mkdir nexus
cd nexus
wget -O nexus.tar.gz http://download.sonatype.com/nexus/3/nexus-${1}-unix.tar.gz
tar -xvf nexus.tar.gz
mv nexus-3* nexus

mkdir debian

#create changelog
dch --create -v ${1} --package nexus "Created from  nexus-${1}-unix.tar.gz via nexusdeb.sh"

echo "Source: nexus" > debian/control
echo "Maintainer: $2" >> debian/control
echo "Section: misc" >> debian/control
echo "Priority: optional" >> debian/control
echo "Standards-Version: 4.3.0" >> debian/control
echo "Build-Depends: debhelper-compat (= 10)" >> debian/control
echo "Homepage: http://www.sonatype.org/" >> debian/control
echo "" >> debian/control
echo "Package: nexus" >> debian/control
echo "Architecture: any" >> debian/control
echo "Depends: adoptopenjdk-8-hotspot-jre, \${misc:Depends}" >> debian/control
echo "Description: Repository manager for development teams." >> debian/control
echo " Nexus sets the standard for repository management providing" >> debian/control
echo " development teams with the ability to proxy remote" >> debian/control
echo " repositories and share software artifacts." >> debian/control

echo "Sonatype Nexus™ Open Source Version" >> debian/copyright
echo "" >> debian/copyright
echo "Copyright © 2008-2012 Sonatype, Inc." >> debian/copyright
echo "" >> debian/copyright
echo "All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions." >> debian/copyright
echo "This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0, which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html." >> debian/copyright
echo "" >> debian/copyright
echo 'Sonatype Nexus™ Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the Eclipse Foundation. All other trademarks are the property of their respective owners.' >> debian/copyright

mv nexus/etc/ .
mv nexus/bin/nexus.vmoptions .

echo "etc/ etc/nexus" >> debian/nexus.install
echo "nexus.vmoptions /etc/nexus/" >> debian/nexus.install
echo "nexus/ var/lib/nexus" >> debian/nexus.install
echo "sonatype-work var/lib/" >> debian/nexus.install
echo "/etc/nexus /var/lib/nexus/etc" >> debian/nexus.links
echo "/etc/nexus/nexus.vmoptions /var/lib/nexus/bin/nexus.vmoptions" >> debian/nexus.links

cat << EOF >> debian/nexus.postinst
set -e
# Add nexus user
if ! getent passwd nexus > /dev/null; then
    adduser --quiet --system --home /var/lib/nexus \
        --group --gecos "Nexus user" nexus || true
fi

# Set ownership
chown -R nexus:nexus /var/lib/nexus
chown -R nexus:nexus /var/lib/sonatype-work
sudo chown -R nexus:nexus /etc/nexus
EOF

echo "#!/usr/bin/make -f" >> debian/rules
echo "%:" >> debian/rules
echo -e "\tdh \$@" >> debian/rules

mkdir -p debian/source
echo "3.0 (quilt)" > debian/source/format

dpkg-buildpackage -b -rfakeroot -us -uc
#debuild -us -uc

#clean up
cd ..
rm -rf nexus 
rm *.buildinfo || true
rm *.changes || true
rm *.tar.gz || true
rm *.dsc || true
